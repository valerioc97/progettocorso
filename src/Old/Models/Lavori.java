package Old.Models;

import java.util.List;

public class Lavori {

    private String tipoRuolo;
    private String nomeAzienda;
    private List<Annualita> periodoAnno;

    public String getTipoRuolo() {
        return tipoRuolo;
    }

    public Lavori setTipoRuolo(String tipoRuolo) {
        this.tipoRuolo = tipoRuolo;
        return this;
    }

    public String getNomeAzienda() {
        return nomeAzienda;
    }

    public Lavori setNomeAzienda(String nomeAzienda) {
        this.nomeAzienda = nomeAzienda;
        return this;
    }

    public List<Annualita> getPeriodoAnno() {
        return periodoAnno;
    }

    public Lavori setPeriodoAnno(List<Annualita> periodoAnno) {
        this.periodoAnno = periodoAnno;
        return this;
    }

    @Override
    public String toString() {
        return "Lavori{" +
                "tipoRuolo='" + tipoRuolo + '\'' +
                ", nomeAzienda='" + nomeAzienda + '\'' +
                ", periodoAnno=" + periodoAnno +
                '}';
    }
}

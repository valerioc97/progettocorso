package Old.Models;

public class Annualita {
    private String anno;

    public String getAnno() {
        return anno;
    }

    public Annualita setAnno(String anno) {
        this.anno = anno;
        return this;
    }

    @Override
    public String toString() {
        return "Annualita{" +
                "anno='" + anno + '\'' +
                '}';
    }
}

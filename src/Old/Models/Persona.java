package Old.Models;

import java.util.List;

public class Persona {
    private String nome;
    private String cognome;
    private String eta;
    private List<Lavori> lavori;

    public String getNome() {
        return nome;
    }

    public Persona setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getCognome() {
        return cognome;
    }

    public Persona setCognome(String cognome) {
        this.cognome = cognome;
        return this;
    }

    public String getEta() {
        return eta;
    }

    public Persona setEta(String eta) {
        this.eta = eta;
        return this;
    }

    public List<Lavori> getLavori() {
        return lavori;
    }

    public Persona setLavori(List<Lavori> lavori) {
        this.lavori = lavori;
        return this;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "nome='" + nome + '\'' +
                ", cognome='" + cognome + '\'' +
                ", eta='" + eta + '\'' +
                ", lavori=" + lavori +
                '}';
    }
}

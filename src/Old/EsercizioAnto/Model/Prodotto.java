package Old.EsercizioAnto.Model;

public class Prodotto implements iProdotto {
    private Integer id;
    private String nome;
    private String descrizione;
    private Integer prezzo;

    public Prodotto(Integer id, String nome, String descrizione, Integer prezzo) {
        this.id = id;
        this.nome = nome;
        this.descrizione = descrizione;
        this.prezzo = prezzo;
    }

    public Integer getId() {
        return id;
    }

    public Prodotto setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public Prodotto setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public Prodotto setDescrizione(String descrizione) {
        this.descrizione = descrizione;
        return this;
    }

    public Integer getPrezzo() {
        return prezzo;
    }

    public Prodotto setPrezzo(Integer prezzo) {
        this.prezzo = prezzo;
        return this;
    }

    @Override
    public String toString() {
        return "Prodotto{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", descrizione='" + descrizione + '\'' +
                ", prezzo=" + prezzo +
                '}';
    }

    @Override
    public Prodotto stampaInfo() {
        return new Prodotto(id, nome, descrizione, prezzo);
    }
}

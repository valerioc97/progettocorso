package Old.EsercizioAnto.Model;

public class Scarpe extends Prodotto{

    private String colore;
    private Integer numero;
    private String marca;

    public Scarpe(Integer id, String nome, String descrizione, Integer prezzo, String colore, Integer numero, String marca) {
        super(id, nome, descrizione, prezzo);
        this.colore = colore;
        this.numero = numero;
        this.marca = marca;
    }

    public String getColore() {
        return colore;
    }

    public Scarpe setColore(String colore) {
        this.colore = colore;
        return this;
    }

    public Integer getNumero() {
        return numero;
    }

    public Scarpe setNumero(Integer numero) {
        this.numero = numero;
        return this;
    }

    public String getMarca() {
        return marca;
    }

    public Scarpe setMarca(String marca) {
        this.marca = marca;
        return this;
    }
}

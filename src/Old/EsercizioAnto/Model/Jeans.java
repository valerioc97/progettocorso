package Old.EsercizioAnto.Model;

public class Jeans extends Prodotto{
    private String colore;
    private TagliaEnum taglia;

    public Jeans(Integer id, String nome, String descrizione, Integer prezzo, String colore, TagliaEnum taglia) {
        super(id, nome, descrizione, prezzo);
        this.colore = colore;
        this.taglia = taglia;
    }

    public String getColore() {
        return colore;
    }

    public Jeans setColore(String colore) {
        this.colore = colore;
        return this;
    }

    public TagliaEnum getTaglia() {
        return taglia;
    }

    public Jeans setTaglia(TagliaEnum taglia) {
        this.taglia = taglia;
        return this;
    }
}

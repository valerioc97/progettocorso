package Old.EsercizioAnto.Controller;

import Old.EsercizioAnto.Model.Jeans;
import Old.EsercizioAnto.Model.Prodotto;
import Old.EsercizioAnto.Model.Scarpe;
import Old.EsercizioAnto.Model.TagliaEnum;

import java.util.Scanner;

public class MainProdottoFixed {

    public static void main(String[] args) {
        Scarpe sc1 = new Scarpe(10, "Jordan", "Scarpetta Ginnastica", 180, "Bianca", 41, "Nike");
        Scarpe sc2 = new Scarpe(22, "K2", "Scarpetta Ginnastica", 120, "Blu e Rossa", 38, "Adidas");
        Scarpe sc3 = new Scarpe(1, "Stars", "Scarpetta Ginnastica", 85, "Nera", 44, "Puma");
        Jeans j1 = new Jeans(8, "Blu Sky", "SlimFit", 110, "Blu scuro", TagliaEnum.M);
        Jeans j2 = new Jeans(14, "Black Shadow", "Slim", 100, "Nero", TagliaEnum.XL);

        Prodotto[] prodotti = {sc1, sc2, sc3, j1, j2};

        Scanner input = new Scanner(System.in);

        double importoTot = 0;
        Boolean flg = false;

        while (true) {
            System.out.println("Vuoi acquistare un prodotto? Y/N");
            String risp = input.nextLine();

            if (risp.equalsIgnoreCase("Y")) {
                for (int i = 0; i < prodotti.length; i++) {
                    System.out.println(prodotti[i].stampaInfo());
                }

                System.out.println("Inserisci l'id del prodotto che vuoi acquistare: ");
                int idScelto = Integer.parseInt(input.nextLine());
                for (int i = 0; i < prodotti.length; i++) {
                    if (idScelto == prodotti[i].getId()) {
                        System.out.println(prodotti[i].getId());
                        importoTot += prodotti[i].getPrezzo();
                        flg = true;
                        break;
                    } else {
                        flg = false;
                    }
                }
                if (!flg) {
                    System.out.println("Prodotto con id: " + idScelto + " non trovato");
                }
            } else if (risp.equalsIgnoreCase("N")) {
                System.out.println("Arrivederci, hai speso: " + importoTot);
                input.close();
                break;
            }
        }

    }

}

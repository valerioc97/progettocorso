package Old.Model;

import java.math.BigDecimal;

public class ImportoApi {

	private BigDecimal valore;
	private String codDivisa;

	public BigDecimal getValore() {
		return valore;
	}
	public String getCodDivisa() {
		return codDivisa;
	}
	public ImportoApi setValore(BigDecimal valore) {
		this.valore = valore;
		return this;
	}
	public ImportoApi setCodDivisa(String codDivisa) {
		this.codDivisa = codDivisa;
		return this;
	}

	@Override
	public String toString() {
		return "ImportoApi{" +
				"valore=" + valore +
				", codDivisa='" + codDivisa + '\'' +
				'}';
	}
}

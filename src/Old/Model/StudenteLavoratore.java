package Old.Model;

public class StudenteLavoratore extends Studente{

    private String professione;

    public String getProfessione() {
        return professione;
    }

    public StudenteLavoratore setProfessione(String professione) {
        this.professione = professione;
        return this;
    }
}

package Old.Model;

import java.util.List;

public class IntegersList {
    List<Integer> list;

    public List<Integer> getList() {
        return list;
    }

    public IntegersList setList(List<Integer> list) {
        this.list = list;
        return this;
    }

    @Override
    public String toString() {
        return "IntegersList{" +
                "list=" + list +
                '}';
    }
}

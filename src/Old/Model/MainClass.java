package Old.Model;

import java.util.List;

public class MainClass {
    List<IntegersList> integersListList;

    public List<IntegersList> getIntegersListList() {
        return integersListList;
    }

    public MainClass setIntegersListList(List<IntegersList> integersListList) {
        this.integersListList = integersListList;
        return this;
    }

    @Override
    public String toString() {
        return "MainClass{" +
                "integersListList=" + integersListList +
                '}';
    }
}

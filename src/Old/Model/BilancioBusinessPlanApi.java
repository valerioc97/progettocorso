package Old.Model;

import java.util.List;

public class BilancioBusinessPlanApi {
    private Integer numAnno;
    private CodTipoBilancioAssumptionEnum tipo;
    private List<VoceBilancioBPApi> vociBilancio;

    public Integer getNumAnno() {
        return numAnno;
    }

    public BilancioBusinessPlanApi setNumAnno(Integer numAnno) {
        this.numAnno = numAnno;
        return this;
    }

    public CodTipoBilancioAssumptionEnum getTipo() {
        return tipo;
    }

    public BilancioBusinessPlanApi setTipo(CodTipoBilancioAssumptionEnum tipo) {
        this.tipo = tipo;
        return this;
    }

    public List<VoceBilancioBPApi> getVociBilancio() {
        return vociBilancio;
    }

    public BilancioBusinessPlanApi setVociBilancio(List<VoceBilancioBPApi> vociBilancio) {
        this.vociBilancio = vociBilancio;
        return this;
    }

    @Override
    public String toString() {
        return "BilancioBusinessPlanApi{" +
                "numAnno=" + numAnno +
                ", tipo=" + tipo +
                ", vociBilancio=" + vociBilancio +
                '}';
    }
}


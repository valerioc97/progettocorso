package Old.Model;

import java.time.LocalDateTime;

public class VoceBilancioBPApi {
    private String codItem;
    private ImportoApi impValore;
    private Integer numProgItem;
    private LocalDateTime tmsUltimaModificaFonte;


    public String getCodItem() {
        return codItem;
    }

    public VoceBilancioBPApi setCodItem(String codItem) {
        this.codItem = codItem;
        return this;
    }

    public ImportoApi getImpValore() {
        return impValore;
    }

    public VoceBilancioBPApi setImpValore(ImportoApi impValore) {
        this.impValore = impValore;
        return this;
    }

    public Integer getNumProgItem() {
        return numProgItem;
    }

    public VoceBilancioBPApi setNumProgItem(Integer numProgItem) {
        this.numProgItem = numProgItem;
        return this;
    }

    public LocalDateTime getTmsUltimaModificaFonte() {
        return tmsUltimaModificaFonte;
    }

    public VoceBilancioBPApi setTmsUltimaModificaFonte(LocalDateTime tmsUltimaModificaFonte) {
        this.tmsUltimaModificaFonte = tmsUltimaModificaFonte;
        return this;
    }

    @Override
    public String toString() {
        return "VoceBilancioBPApi{" +
                "codItem='" + codItem + '\'' +
                ", impValore=" + impValore +
                ", numProgItem=" + numProgItem +
                ", tmsUltimaModificaFonte=" + tmsUltimaModificaFonte +
                '}';
    }
}


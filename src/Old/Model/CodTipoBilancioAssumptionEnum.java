package Old.Model;

public enum CodTipoBilancioAssumptionEnum {

    PREVISIONALE("PREVISIONALE"),

    STORICO("STORICO"),

    ASSUMPTION("ASSUMPTION"),

    CEBI("CEBI"),

    CUMULATO("CUMULATO");

    private String value;

    CodTipoBilancioAssumptionEnum(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static CodTipoBilancioAssumptionEnum fromValue(String text) {
        for (CodTipoBilancioAssumptionEnum b : CodTipoBilancioAssumptionEnum.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }
}

package Old.Model;

public class Studente {
    private static final String nome = "Valerio";
    private Boolean fuoriCorso = false;

    public static String getNome() {
        return nome;
    }

    public Boolean getFuoriCorso() {
        return fuoriCorso;
    }

    public Studente setFuoriCorso(Boolean fuoriCorso) {
        this.fuoriCorso = fuoriCorso;
        return this;
    }
}

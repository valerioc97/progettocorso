package Old.Proof;

public class Autore {
    private String nome;
    private String cognome;

    public String getNome() {
        return nome;
    }

    public Autore setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getCognome() {
        return cognome;
    }

    public Autore setCognome(String cognome) {
        this.cognome = cognome;
        return this;
    }

    @Override
    public String toString() {
        return "Autore{" +
                "nome='" + nome + '\'' +
                ", cognome='" + cognome + '\'' +
                '}';
    }
}

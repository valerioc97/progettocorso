package Old.Proof;

public class Libro {
    private Autore autore;
    private Genere genere;

    public Autore getAutore() {
        return autore;
    }

    public Libro setAutore(Autore autore) {
        this.autore = autore;
        return this;
    }

    public Genere getGenere() {
        return genere;
    }

    public Libro setGenere(Genere genere) {
        this.genere = genere;
        return this;
    }
}

package Old.Proof;

public class Genere extends Autore {
    private String genere;

    public String getGenere() {
        return genere;
    }

    public Genere setGenere(String genere) {
        this.genere = genere;
        return this;
    }

    @Override
    public String toString() {
        return "Genere{" +
                "genere='" + genere + '\'' +
                '}';
    }
}

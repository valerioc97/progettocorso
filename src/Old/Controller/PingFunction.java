package Old.Controller;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class PingFunction {

    public String pingFunction() throws IOException {
        String var = "https://bitbucket.intesasanpaolo.com/";
        String var1 = "www.google.it";

        InetAddress inetAddress = InetAddress.getByName(var1);
        return inetAddress.isReachable(5000) ? "REACHABLE" : "NONE";
    }
}

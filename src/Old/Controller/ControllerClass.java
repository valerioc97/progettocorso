package Old.Controller;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ControllerClass {
    public static void main(String[] args) {
        List<Integer> listOfInteger = Arrays.asList(5, 5, 5, 5, 1);
        Set<Integer> setOfInteger = new HashSet<>(listOfInteger);

        System.out.println(listOfInteger);
        System.out.println(setOfInteger);

        if (setOfInteger.size() > 1) {
            System.out.println("Non ci sono duplicati");
        } else if (setOfInteger.size() == 1) {
            System.out.println("Ci sono dei duplicati");
        }

        Long count = listOfInteger.stream().distinct().count();

        System.out.println(count);
    }
}

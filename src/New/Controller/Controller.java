package New.Controller;

import New.Methods.MethodsClass;
import New.Methods.ModelTemp;

import java.nio.file.Path;
import java.util.List;

public class Controller {

    public static void main(String[] args) {
        // Declarations
        List<String> list;
        List<String[]> listOfList;

        // STEPS - STARTS
        List<Path> urls = MethodsClass.loopOnDirectory();

        if (urls != null && !urls.isEmpty()) {
            for (Path url : urls) {
                listOfList = MethodsClass.readingFile(url);
                ModelTemp mt = MethodsClass.extractHeaderAndRows(listOfList);
                MethodsClass.mappingValueCanale(mt);
                MethodsClass.mappingValueTipoFirma(mt);
                list = MethodsClass.removeUselessFields(mt.getHeader(), mt.getRows());
                list = MethodsClass.fixValueForSTATO_PROCESSO(list);
                list = MethodsClass.fixValueForCOD_CLIENTE_TITOLARE(list);
                list = MethodsClass.fieldsRenaming(list);
                list = MethodsClass.mappingValueStatoProcesso(list);
                // STEPS - ENDS

                // Writing final object into csv with semicolon
                MethodsClass.writingIntoCSV(list, url.getFileName());
            }
        }


    }
}

package New.Models;

public class Studente extends Object{

    private Integer cfu;
    private String facolta;

    public Integer getCfu() {
        return cfu;
    }

    public Studente setCfu(Integer cfu) {
        this.cfu = cfu;
        return this;
    }

    public String getFacolta() {
        return facolta;
    }

    public Studente setFacolta(String facolta) {
        this.facolta = facolta;
        return this;
    }
}

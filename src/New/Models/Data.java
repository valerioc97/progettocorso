package New.Models;

public class Data {

    private Integer giorno;
    private Integer mese;
    private Integer anno;

    public Integer getGiorno() {
        return giorno;
    }

    public Data setGiorno(Integer giorno) {
        if (giorno != null && giorno >= 1 && giorno <= 31) {
            this.giorno = giorno;
        } else {
            System.out.println("Giorno non valido");
        }
        return this;
    }

    public Integer getMese() {
        return mese;
    }

    public Data setMese(Integer mese) {
        if (mese != null && mese >= 1 && giorno <= 12) {
            this.mese = mese;
        } else {
            System.out.println("Mese non valido");
        }
        return this;
    }

    public Integer getAnno() {
        return anno;
    }

    public Data setAnno(Integer anno) {
        if (anno != null && anno >= 1900) {
            this.anno = anno;
        } else {
            System.out.println("Anno non valido");
        }
        return this;
    }
}

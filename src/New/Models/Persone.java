package New.Models;

public class Persone extends Professore{

    private String nome;
    private String cognome;

    public String getNome() {
        return nome;
    }

    public Persone setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getCognome() {
        return cognome;
    }

    public Persone setCognome(String cognome) {
        this.cognome = cognome;
        return this;
    }

    @Override
    public String toString() {
        return "Persone{" +
                "nome='" + nome + '\'' +
                ", cognome='" + cognome + '\'' +
                '}';
    }
}

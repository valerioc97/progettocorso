package New.Models;

public class Punto {
    private Integer x;
    private Integer y;

    public Integer getX() {
        return x != null ? x : 0;
    }

    public Punto setX(Integer x) {
        this.x = x;
        return this;
    }

    public Integer getY() {
        return y != null ? y : 0;
    }

    public Punto setY(Integer y) {
        this.y = y;
        return this;
    }
}

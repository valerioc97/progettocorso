package New.Models;

public class Professore extends Studente{

    private Integer stipendio;
    private String materiaDiStudio;

    public Integer getStipendio() {
        return stipendio;
    }

    public Professore setStipendio(Integer stipendio) {
        this.stipendio = stipendio;
        return this;
    }

    public String getMateriaDiStudio() {
        return materiaDiStudio;
    }

    public Professore setMateriaDiStudio(String materiaDiStudio) {
        this.materiaDiStudio = materiaDiStudio;
        return this;
    }
}

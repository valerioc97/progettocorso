package New.Methods;

import java.util.ArrayList;
import java.util.List;

public class ModelTemp {
    List<String> header;
    List<List<String>> rows;

    public List<String> getHeader() {
        if(header == null){
            header = new ArrayList<>();
        }
        return header;
    }

    public ModelTemp setHeader(List<String> header) {
        this.header = header;
        return this;
    }

    public List<List<String>> getRows() {
        if(rows == null){
            rows = new ArrayList<>();
        }
        return rows;
    }

    public ModelTemp setRows(List<List<String>> rows) {
        this.rows = rows;
        return this;
    }
}

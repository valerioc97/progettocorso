package New.Methods;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MethodsClass {
    /*
    Costruttore privato per evitarne l'istanza ed utilizzare i metodi statici correttamente.
     */
    private MethodsClass() {
        // Do Nothing
    }

    public static void writingIntoCSV(List<String> tempList, Path url) {

        int size = String.valueOf(url).length() - 4;
        String name = String.valueOf(url).substring(0, size) + "_EXPORTED" + ".csv";

        try (FileWriter file = new FileWriter("C:/Users/valerio.carcaterra/Documents/proof/" + name)) {

            StringBuilder stringBuilder = new StringBuilder();
            for (String s : tempList) {
                if (!s.contains("\r\n")) {
                    stringBuilder.append(s).append(";");
                } else {
                    stringBuilder.append(s);
                }

            }
            List<StringBuilder> stringBuilderList = new ArrayList<>();
            stringBuilderList.add(stringBuilder);
            for (StringBuilder stb : stringBuilderList) {
                file.write(String.valueOf(stb));
            }
            System.out.println("[!] File " + name + " has been created");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<String> fieldsRenaming(List<String> csv) {
        List<String> tempList = new ArrayList<>();
        for (String s : csv) {
            if (s.contains("ID_SUPER_PRATICA")) {
                String replaced = s.replace("ID_SUPER_PRATICA", "ID_SUPERPRATICA");
                tempList.add(replaced);
            } else if (s.contains("SP.COD_STATO||'-'||S.DESC_STATO")) {
                String replaced = s.replace("SP.COD_STATO||'-'||S.DESC_STATO", "STATO_PROCESSO");
                tempList.add(replaced);
            } else if (s.contains("STATO")) {
                String replaced = s.replace("STATO", "STATO_PROCESSO");
                tempList.add(replaced);
            } else if (s.contains("DATA_CREAZIONE")) {
                String replaced = s.replace("DATA_CREAZIONE", "DATA_INIZIO");
                tempList.add(replaced);
            } else if (s.contains("DATA_AVANZAMENTO")) {
                String replaced = s.replace("DATA_AVANZAMENTO", "DATA_FINE");
                tempList.add(replaced);
            } else if (s.contains("COD_BT")) {
                String replaced = s.replace("COD_BT", "COD_CLIENTE_TITOLARE");
                tempList.add(replaced);
            } else if (s.contains("CAT_PRODOTTO")) {
                String replaced = s.replace("CAT_PRODOTTO", "CATEGORIA_PRODOTTO");
                tempList.add(replaced);
            } else if (s.contains("TIP_OFFERTA")) {
                String replaced = s.replace("TIP_OFFERTA", "CANALE");
                tempList.add(replaced);
            } else if (s.contains("MODALITA_FIRMA_PROPOSTA")) {
                String replaced = s.replace("MODALITA_FIRMA_PROPOSTA", "TIPO_FIRMA");
                tempList.add(replaced);
            } else if (s.contains("TIPO_OPERAZIONE_SP")) {
                String replaced = s.replace("TIPO_OPERAZIONE_SP", "TIPO_PROCESSO");
                tempList.add(replaced);
            } else if (s.contains("CIB_OFFERTA_BASE")) {
                String replaced = s.replace("CIB_OFFERTA_BASE", "CODICE_OFFERTA_BASE");
                tempList.add(replaced);
            } else if (s.contains("CIB_ELENCO_CREDENZIALI_FIRMAT")) {
                String replaced = s.replace("CIB_ELENCO_CREDENZIALI_FIRMAT", "CREDENZIALI_FIRMATARI");
                tempList.add(replaced);
            } else {
                tempList.add(s);
            }
        }
        return tempList;

    }

    public static List<String> fixValueForCOD_CLIENTE_TITOLARE(List<String> list) {
        List<String> tempList = new ArrayList<>();
        for (String s : list) {
            if (s.startsWith("S_")) {
                String valueReplaced = s.substring(2);
                tempList.add(valueReplaced);
            } else {
                tempList.add(s);
            }
        }
        return tempList;

    }

    public static List<String> fixValueForSTATO_PROCESSO(List<String> list) {
        List<String> tempList = new ArrayList<>();
        for (String s : list) {
            if (s.contains(" - ")) {
                tempList.add(s.substring(7));
            } else {
                tempList.add(s);
            }

        }
        return tempList;
    }

    public static List<String> removeUselessFields(List<String> header, List<List<String>> rows) {
        List<String> finalList = new ArrayList<>();

        int posCodPropostaCanale = 0;
        int posCanaleUltimoAvanz = 0;
        int posElencoFirmatari = 0;
        Boolean flg1 = false;
        Boolean flg2 = false;
        Boolean flg3 = false;
        for (int i = 0; i < header.size(); i++) {
            if (header.get(i).equals("COD_PROPOSTA_CANALE")) {
                posCodPropostaCanale = i;
                flg1 = true;
            }
            if (header.get(i).equals("CANALE_ULTIMO_AVANZAMENTO")) {
                posCanaleUltimoAvanz = i;
                flg2 = true;
            }
            if (header.get(i).equals("ELENCO_FIRMATARI_FIRMA_APPOST")) {
                posElencoFirmatari = i;
                flg3 = true;
            }
        }
        for (List<String> rowses : rows) {
            if (Boolean.TRUE.equals(flg1)) {
                rowses.remove(posCodPropostaCanale);
            }
            if (Boolean.TRUE.equals(flg2)) {
                rowses.remove(posCanaleUltimoAvanz - 1);
            }
            if (Boolean.TRUE.equals(flg3)) {
                rowses.remove(posElencoFirmatari - 1);
            }
        }

        header.removeIf(removing -> removing.equals("ELENCO_FIRMATARI_FIRMA_APPOST") || removing.equals("CANALE_ULTIMO_AVANZAMENTO") || removing.equals("COD_PROPOSTA_CANALE"));

        for (String finalHeader : header) {
            finalList.add(finalHeader);
        }
        finalList.add("\r\n");
        for (List<String> finalRow : rows) {
            finalList.addAll(finalRow);
            finalList.add("\r\n");
        }

        return finalList;

    }

    public static List<String[]> readingFile(Path url) {
        try {

            BufferedReader sc = new BufferedReader(new FileReader(String.valueOf(url)));
            String reading;
            List<String[]> stringArray = new ArrayList<>();
            while ((reading = sc.readLine()) != null) {
                stringArray.add(reading.split(";"));
            }
            return stringArray;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public static ModelTemp extractHeaderAndRows(List<String[]> stringArray) {
        List<String> header = new ArrayList<>(Arrays.asList(stringArray.get(0)));
        List<List<String>> rows = new ArrayList<>();
        ModelTemp modelTemp = new ModelTemp();

        for (String rem : header) {
            stringArray.removeIf(removing -> removing[0].equals(rem));
        }

        for (String[] s : stringArray) {
            List<String> tempList = new ArrayList<>();
            for (String s1 : s) {
                tempList.add(s1);
            }
            rows.add(tempList);
        }
        modelTemp.getHeader().addAll(header);
        modelTemp.getRows().addAll(rows);
        return modelTemp;
    }

    public static String channelMap(String key) {
        Map<String, String> map = new HashMap<>();
        map.put("1", "OAD");
        map.put("4", "SEDE");
        map.put("5", "SELF");

        return map.get(key);
    }

    public static void mappingValueCanale(ModelTemp mt) {

        int posCanaleField = 0;
        Boolean flg = false;
        for (int i = 0; i < mt.getHeader().size(); i++) {
            if (mt.getHeader().get(i).equals("TIP_OFFERTA")) {
                posCanaleField = i;
                flg = true;
            }
        }

        if (Boolean.TRUE.equals(flg)) {
            for (List<String> strings : mt.getRows()) {
                String key = strings.get(posCanaleField);
                strings.remove(posCanaleField);
                strings.add(posCanaleField, channelMap(key));
            }
        }
    }

    public static void mappingValueTipoFirma(ModelTemp mt) {

        int posTipoFirmaField = 0;
        Boolean flg = Boolean.FALSE;
        for (int i = 0; i < mt.getHeader().size(); i++) {
            if (mt.getHeader().get(i).equals("MODALITA_FIRMA_PROPOSTA")) {
                posTipoFirmaField = i;
                flg = true;
            }
        }

        if (Boolean.TRUE.equals(flg)) {
            for (List<String> strings : mt.getRows()) {
                if (strings.size() > posTipoFirmaField && strings.get(posTipoFirmaField).equals("FD")) {
                    strings.remove(posTipoFirmaField);
                    strings.add(posTipoFirmaField, "FDR");
                }
            }
        }
    }

    private static final String ANNULLATO = "Annullato";
    private static final String COMPLETATO = "Completato";
    private static final String IN_CORSO = "In Corso";

    public static Map<String, String> processStatusMap() {
        Map<String, String> map = new HashMap<>();
        map.put("Scaduta", "Scaduto");
        map.put("Annullata", ANNULLATO);
        map.put("Annullato da Operatore", ANNULLATO);
        map.put("Attiva", COMPLETATO);
        map.put("Inviata", COMPLETATO);
        map.put("Valida", COMPLETATO);
        map.put("Firmata parziale", IN_CORSO);
        map.put("In corso", IN_CORSO);
        map.put("In Firma", IN_CORSO);
        map.put("Firmato", IN_CORSO);
        return map;
    }

    public static List<String> mappingValueStatoProcesso(List<String> list) {
        List<String> tempList = new ArrayList<>();
        for (String s : list) {
            if (processStatusMap().containsKey(s)) {
                String val = processStatusMap().get(s);
                tempList.add(val);
            } else {
                tempList.add(s);
            }
        }
        return tempList;
    }

    public static List<Path> loopOnDirectory() {
        try (Stream<Path> paths = Files.walk(Paths.get("C:/Users/valerio.carcaterra/Documents/proof/Files/csv/"))) {

            return paths.filter(Files::isRegularFile).collect(Collectors.toList());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<String> xlsToCsv(Path url) throws IOException {

        BufferedReader sc = new BufferedReader(new FileReader(String.valueOf(url)));
        String reading;
        List<String> stringArray = new ArrayList<>();
        while ((reading = sc.readLine()) != null) {
            stringArray.add(reading);
        }
        return stringArray;
    }
}
